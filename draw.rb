require "curb"
require "json"

tsd = "http://localhost:4242"

http = Curl.get(tsd + "/api/query?m=sum:balcones.moi_backend.response_time&m=sum:balcones.moi_backend.throughput", {
  start: "12h-ago",
  # m: "sum:balcones.moi_backend.response_time",
  # m: "sum:balcones.moi_backend.throughput"
}) do |curl|
  curl.verbose = true
end
response = JSON.parse(http.body_str)

response.each do |metric|
  puts metric["metric"]
  metric["dps"].each do |k, v|
    puts "#{Time.at k.to_i}: #{v}"
  end
end
