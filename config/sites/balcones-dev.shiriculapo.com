server {
  listen 80;

  server_name balcones-dev.shiriculapo.com
              www.balcones-dev.shiriculapo.com;

  root /home/balcones/balcones/current/public;

  passenger_enabled on;
  passenger_app_env production;
  passenger_user balcones;
  passenger_ruby /home/balcones/.rbenv/versions/2.3.1/bin/ruby;

  location ^~ /assets/ {
    gzip_static on;
    expires max;
    add_header Cache-Control public;
  }

  location /tsd/ {
    proxy_pass http://localhost:4242/;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
  }

  error_page 500 502 503 504 /500.html;
  keepalive_timeout 10;
  client_max_body_size 4G;
}
