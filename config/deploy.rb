# config valid only for current version of Capistrano
lock "3.8.2"

set :application, "balcones"
set :repo_url, "git@bitbucket.org:macool/balcones.git"

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :branch, ENV['BRANCH'] || 'master'

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/home/balcones/balcones"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"
append :linked_files, '.env'

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'monit_configs'

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :bundle_without, %w{development test deployment}.join(' ')

set :rbenv_type, :user
set :rbenv_ruby, File.read('.ruby-version').strip

set :passenger_restart_with_touch, true

set :default_env, {
  'RAILS_ENV' => fetch(:stage)
}

# set :hipchat_token, "297aa5ed53adeb2638c16eeb1b3f25"
# set :hipchat_room_name, "balcones"
