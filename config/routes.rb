require 'sidekiq/web'

Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :dashboard,
            path: 'd',
            only: :show do
    resources :servers, only: :show
    resources :keepalives, only: :show
    resources :applications, only: :show
  end
  authenticate :admin_user do
    mount Sidekiq::Web => '/admin/sidekiq'
  end
end
