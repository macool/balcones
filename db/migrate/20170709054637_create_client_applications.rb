class CreateClientApplications < ActiveRecord::Migration[5.1]
  def change
    create_table :client_applications do |t|
      t.string :name, null: false
      t.references :client, foreign_key: true, null: false
      t.integer :newrelic_app_id

      t.timestamps
    end
  end
end
