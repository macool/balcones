class AddIndexToClients < ActiveRecord::Migration[5.1]
  def change
    add_index :clients, :name
  end
end
