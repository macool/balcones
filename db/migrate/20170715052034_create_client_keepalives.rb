class CreateClientKeepalives < ActiveRecord::Migration[5.1]
  def change
    create_table :client_keepalives do |t|
      t.references :client, foreign_key: true, index: true, null: false
      t.string :name, null: false
      t.string :uri
      t.string :searchterm
      t.string :status

      t.timestamps
    end
  end
end
