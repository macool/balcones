class CreateClientServers < ActiveRecord::Migration[5.1]
  def change
    create_table :client_servers do |t|
      t.references :client, foreign_key: true, index: true, null: false
      t.string :name, null: false
      t.integer :newrelic_server_id

      t.timestamps
    end
  end
end
