require "curb"
require "json"
# require "opentsdb"

timeout = 10

def persist_data(application)
  # tsd = OpenTSDB::Client.new

  application["application_summary"].each do |key, value|
    data = {
      metric: "balcones.moi_backend.#{key}",
      value: value,
      timestamp: Time.now.to_i,
      tags: {
        reporting: (application["reporting"] ? 'yes' : 'no'),
        health_status: application["health_status"]
      }
    }

    # tsd.put data
    http = Curl.post("http://localhost:4242/api/put", data.to_json) do |curl|
      # curl.verbose = true
    end
    puts http.body_str
  end
end

def perform_query
  app_id = 14286445
  http = Curl.get("https://api.newrelic.com/v2/applications/#{app_id}.json") do |http|
    http.headers['X-Api-Key'] = 'acf828ca7ca81829042cc2f0e047b71ccc6fa1154aea148'
  end
  response = JSON.parse(http.body_str)
  persist_data(response["application"])
end

while true
  perform_query
  puts "sleeping #{timeout} secs"
  sleep timeout # secs
end
