class ClientApplication < ApplicationRecord
  TSD_PREFIX = "balcones.application.".freeze

  belongs_to :client
  validates :client, :name, presence: true

  scope :active, -> { where.not(newrelic_app_id: nil) }
end
