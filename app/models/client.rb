class Client < ApplicationRecord
  validates :name, presence: true,
                   uniqueness: true
  has_many :servers, class_name: "ClientServer"
  has_many :keepalives, class_name: "ClientKeepalive"
  has_many :applications, class_name: "ClientApplication"

  scope :by_name, ->(name) { find_by(name: name) }
end
