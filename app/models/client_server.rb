class ClientServer < ApplicationRecord
  TSD_PREFIX = "balcones.server.".freeze

  belongs_to :client

  validates :name, presence: true

  scope :active, -> { where.not(newrelic_server_id: nil) }
end
