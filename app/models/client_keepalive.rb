class ClientKeepalive < ApplicationRecord
  extend Enumerize

  belongs_to :client

  validates :client,
            :uri,
            :name,
            presence: true

  scope :active, -> { where.not(uri: nil) }

  enumerize :status, in: [:up, :error, :down], default: :down

  def schedule_ping!
    KeepalivePingWorker.perform_async(id)
  end
end
