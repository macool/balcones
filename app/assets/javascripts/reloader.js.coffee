performReload = ->
  window.location.reload()

jQuery ->
  seconds = 60 * 5
  setTimeout(performReload, seconds * 1000)
