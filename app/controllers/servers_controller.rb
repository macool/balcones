class ServersController < ApplicationController
  def show
    @client = Client.by_name(params[:dashboard_id])
    @server = @client.servers.find params[:id]
    @chart = TsdChart::ClientServerChart.new @server
  end
end
