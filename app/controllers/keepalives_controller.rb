class KeepalivesController < ApplicationController
  def show
    @client = Client.by_name(params[:dashboard_id])
    @keepalive = @client.keepalives.find params[:id]
    @chart = TsdChart::ClientKeepaliveChart.new @keepalive
  end
end
