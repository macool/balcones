class ApplicationsController < ApplicationController
  def show
    @client = Client.by_name(params[:dashboard_id])
    @application = @client.applications.find params[:id]
    @chart = TsdChart::ClientApplicationChart.new @application
  end
end
