class DashboardController < ApplicationController
  def show
    @client = Client.by_name(params[:id])
    @applications = @client.applications
    @servers = @client.servers
    @keepalives = @client.keepalives
  end

  private

  def application_chart_for(application)
    TsdChart::ClientApplicationChart.new(application)
  end
  helper_method :application_chart_for

  def server_chart_for(server)
    TsdChart::ClientServerChart.new(server)
  end
  helper_method :server_chart_for

  def keepalive_chart_for(keepalive)
    TsdChart::ClientKeepaliveChart.new(keepalive)
  end
  helper_method :keepalive_chart_for
end
