class KeepalivePingWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(keepalive_id)
    keepalive = ClientKeepalive.find(keepalive_id)
    KeepalivePingService.new(keepalive).ping!
  end
end
