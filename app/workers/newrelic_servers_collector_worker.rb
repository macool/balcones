class NewrelicServersCollectorWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    NewrelicServerCollectorService.collect_from_all!
  end
end
