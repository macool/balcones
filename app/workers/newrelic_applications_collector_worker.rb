class NewrelicApplicationsCollectorWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    NewrelicApplicationCollectorService.collect_from_all!
  end
end
