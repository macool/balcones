class ClientKeepalivesCollectorWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    ClientKeepalive.active.find_each do |keepalive|
      keepalive.schedule_ping!
    end
  end
end
