class KeepalivePingService
  FAILURE_DELAY = 1.minute

  attr_reader :keepalive, :endpoint_response, :get_exception

  def initialize(keepalive)
    @keepalive = keepalive
  end

  def ping!
    get_endpoint
    tsd_store!
    if success?
      ping_succeeded
    else
      handle_failure
    end
  end

  def success?
    response_ok? && includes_searchterm?
  end

  private

  def get_endpoint
    @endpoint_response = Curl.get(@keepalive.uri) do |curl|
      curl.follow_location = true
      curl.max_redirects = 3
    end
  rescue Curl::Err::CurlError => e
    @get_exception = e
  end

  def response_ok?
    @endpoint_response.present? && @endpoint_response.response_code == 200
  end

  def includes_searchterm?
    @endpoint_response.body.include? @keepalive.searchterm
  end

  def ping_succeeded
    if @keepalive.status.down?
      trigger_alerts! :up
    end
    @keepalive.update! status: :up
  end

  def tsd_store!
    Tsd::ClientKeepaliveStore.new(ping_service: self).persist!
  end

  def handle_failure
    ##
    # 1st keepalive goes from up to error and schedules a retry
    # 2nd keepalive goes from error to down and triggers alerts
    if @keepalive.status.up?
      reschedule_ping!
    elsif @keepalive.status.error?
      trigger_alerts! :down
      @keepalive.update!(status: :down)
    end
  end

  def reschedule_ping!
    @keepalive.update!(status: :error)
    KeepalivePingWorker.perform_in(FAILURE_DELAY, @keepalive.id)
  end

  def trigger_alerts!(status)
    return if @keepalive.slackwebhook.blank?
    slack_notifier = AlertService::SlackNotifier.new(@keepalive.slackwebhook)
    slack_notifier.notify!(slack_notification_for(status))
  end

  def slack_notification_for(status)
    text = I18n.t("alerts.keepalive.status", name: @keepalive.name, status: status)
    {
      text: text,
      attachments: [ {
        text: text,
        color: I18n.t("alerts.keepalive.color.#{status}")
      } ]
    }
  end
end
