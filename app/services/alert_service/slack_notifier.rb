class AlertService::SlackNotifier
  def initialize(slack_url)
    @notifier = Slack::Notifier.new(slack_url)
  end

  def notify!(notification)
    @notifier.ping(notification)
  end
end
