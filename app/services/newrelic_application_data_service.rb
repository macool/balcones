class NewrelicApplicationDataService
  def initialize(app:, response:)
    @app = app
    @response_app = response.parsed_response["application"]
    @metric_prefix = ClientApplication::TSD_PREFIX
  end

  def persist!
    return false if app_not_reporting? || already_saved_data?
    @response_app["application_summary"].each do |key, value|
      tsd.put(
        metric: "#{@metric_prefix}#{key}",
        value: value,
        timestamp: last_reported_at.to_i,
        tags: {
          app_id: @app.id,
          reporting: (@response_app["reporting"] ? 'yes' : 'no'),
          health_status: @response_app["health_status"]
        }
      )
    end
  end

  private

  def last_reported_at
    if @response_app["last_reported_at"].present?
      Time.parse(@response_app["last_reported_at"])
    end
  end

  def app_not_reporting?
    not_reporting = @response_app["reporting"] == false
    Airbrake.notify('app not reporting!', app_id: @app.id) if not_reporting
    not_reporting
  end

  ##
  # query tsd for current timestamp
  def already_saved_data?
    metric = @response_app["application_summary"].keys.first
    prefix = ClientApplication::TSD_PREFIX
    response = tsd.query_last(prefix + metric).parsed_response
    return false if response.is_a?(Hash) && response["error"].present?
    response.first && response.first["timestamp"] == last_reported_at.to_i
  end

  def tsd
    @tsd ||= TsdApi.new
  end
end
