class NewrelicApi
  include HTTParty
  base_uri "https://api.newrelic.com/v2"

  def get_server(id)
    self.class.get(
      "/servers/#{id}.json",
      headers: default_headers
    )
  end

  def get_application(id)
    self.class.get(
      "/applications/#{id}.json",
      headers: default_headers
    )
  end

  private

  def default_headers
    {
      'X-Api-Key' => Rails.application.secrets.newrelic_api_key
    }
  end
end
