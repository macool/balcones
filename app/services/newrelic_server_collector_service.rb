class NewrelicServerCollectorService
  class << self
    def collect_from_all!
      ClientServer.active.each do |server|
        new(server).collect!
      end
    end
  end

  def initialize(server)
    @server = server
  end

  def collect!
    response = get_newrelic_data
    return if response.parsed_response.is_a?(Hash) && response.parsed_response["error"].present?
    save_newrelic_data(response)
  end

  private

  def get_newrelic_data
    NewrelicApi.new.get_server(@server.newrelic_server_id)
  end

  def save_newrelic_data(response)
    NewrelicServerDataService.new(
      server: @server,
      response: response
    ).persist!
  end
end
