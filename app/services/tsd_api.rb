class TsdApi
  include HTTParty
  base_uri Rails.application.secrets.tsd_host

  def put(data)
    self.class.post("/api/put", body: data.to_json)
  end

  def query_last(timeseries)
    timeseries_str = Array(timeseries).map do |name|
      "timeseries=#{name}"
    end.join("&")
    self.class.get("/api/query/last?#{timeseries_str}")
  end
end
