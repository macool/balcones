class NewrelicApplicationCollectorService
  class << self
    def collect_from_all!
      ClientApplication.active.each do |app|
        new(app).collect!
      end
    end
  end

  def initialize(app)
    @app = app
  end

  def collect!
    response = get_newrelic_data
    return if response.parsed_response.is_a?(Hash) && response.parsed_response["error"].present?
    save_newrelic_data(response)
  end

  private

  def get_newrelic_data
    NewrelicApi.new.get_application(@app.newrelic_app_id)
  end

  def save_newrelic_data(response)
    NewrelicApplicationDataService.new(
      app: @app,
      response: response
    ).persist!
  end
end
