class TsdChart::ClientServerChart < TsdChart::BaseService
  def initialize(server)
    @server = server
  end

  def performance_summary_chart
    chart "cpu", "cpu_stolen", "memory"
  end

  def cpu_chart
    chart "cpu", "cpu_stolen"
  end

  def memory_chart
    chart "memory"
  end

  def memory_absolute_chart
    chart "memory_used", "memory_total"
  end

  def disk_io_chart
    chart "disk_io"
  end

  def disk_chart
    chart "fullest_disk"
  end

  def disk_absolute_chart
    chart "fullest_disk_free"
  end

  private

  def chart(*metrics)
    metrics_str = Array(metrics).map do |metric|
      URI.encode(
        "m=#{CHART_AGGREGATOR}:" + metric_prefix + metric + "{server_id=#{@server.id}}"
      )
    end.join("&")
    "#{tsd_host}/q?start=#{CHART_START}&#{DEFAULT_CHART_OPTS}&#{metrics_str}"
  end

  def metric_prefix
    ClientServer::TSD_PREFIX
  end
end
