class TsdChart::ClientApplicationChart < TsdChart::BaseService
  def initialize(application)
    @application = application
  end

  def rpm_chart
    chart "response_time"
  end

  def throughput_chart
    chart "throughput"
  end

  def instances_chart
    chart "instance_count", "host_count"
  end

  def error_chart
    chart "error_rate"
  end

  def apdex_chart
    chart "apdex_score", "apdex_target"
  end

  private

  def chart(*metrics)
    metrics_str = Array(metrics).map do |metric|
      URI.encode(
        "m=#{CHART_AGGREGATOR}:" + metric_prefix + metric + "{app_id=#{@application.id}}"
      )
    end.join("&")
    "#{tsd_host}/q?start=#{CHART_START}&#{DEFAULT_CHART_OPTS}&#{metrics_str}"
  end

  def metric_prefix
    ClientApplication::TSD_PREFIX
  end
end
