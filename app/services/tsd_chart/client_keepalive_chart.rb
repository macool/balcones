class TsdChart::ClientKeepaliveChart < TsdChart::BaseService
  def initialize(keepalive)
    @keepalive = keepalive
  end

  def load_time_chart
    chart "total_time"
  end

  def availability_chart
    chart "availability"
  end

  def performance_chart
    chart(
      *Tsd::ClientKeepaliveStore::PERFORMANCE_METRICS - secondary_chart_metrics
    )
  end

  def secondary_performance_chart
    chart *secondary_chart_metrics
  end

  private

  def secondary_chart_metrics
    [:total_time, :start_transfer_time].freeze
  end

  def chart(*metrics)
    metrics_str = Array(metrics).map do |metric|
      URI.encode(
        "m=#{CHART_AGGREGATOR}:" + metric_prefix + metric.to_s + "{keepalive_id=#{@keepalive.id}}"
      )
    end.join("&")
    "#{tsd_host}/q?start=#{CHART_START}&#{DEFAULT_CHART_OPTS}&#{metrics_str}"
  end

  def metric_prefix
    Tsd::ClientKeepaliveStore::TSD_PREFIX
  end
end
