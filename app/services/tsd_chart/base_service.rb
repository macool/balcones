class TsdChart::BaseService
  CHART_START = "12h-ago".freeze
  CHART_AGGREGATOR = "max".freeze
  CHART_KEY_OPTS = "out top horiz".freeze
  DEFAULT_CHART_OPTS = "png&wxh=500x260&smooth=csplines&key=#{URI.encode(CHART_KEY_OPTS)}".freeze

  protected

  def tsd_host
    if Rails.env.development?
      Rails.application.secrets.tsd_host
    else
      "/tsd"
    end
  end
end
