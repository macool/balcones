class Tsd::ClientKeepaliveStore
  TSD_PREFIX = "balcones.keepalive.".freeze
  PERFORMANCE_METRICS = [
    :app_connect_time,
    :connect_time,
    :name_lookup_time,
    :pre_transfer_time,
    :redirect_time,
    :start_transfer_time,
    :total_time
  ].freeze

  def initialize(ping_service:)
    @ping_service = ping_service
    @keepalive = @ping_service.keepalive
    @response = @ping_service.endpoint_response
    @error = @ping_service.get_exception
  end

  def persist!
    report_availability!
    if @response.present?
      PERFORMANCE_METRICS.each do |metric_name|
        save_metric(
          name: TSD_PREFIX + metric_name.to_s,
          value: @response.send(metric_name)*1000 #ms
        )
      end
    end
  end

  def report_availability!
    tags = {}
    tags[:error] = @error.class.to_s.parameterize if @error
    value = @ping_service.success? ? 1 : 0
    save_metric({
      value: value,
      name: TSD_PREFIX + "availability"
    }, tags)
  end

  private

  def save_metric(metric, tags = {})
    tsd.put(
      metric: metric[:name],
      value: metric[:value],
      timestamp: Time.now.to_i,
      tags: tags.merge(keepalive_id: @keepalive.id)
    )
  end

  def tsd
    TsdApi.new
  end
end
