class NewrelicServerDataService
  def initialize(server:, response:)
    @server = server
    @response_server = response.parsed_response["server"]
    @metric_prefix = ClientServer::TSD_PREFIX
  end

  def persist!
    return false if server_not_reporting? || already_saved_data?
    @response_server["summary"].each do |key, value|
      tsd.put(
        metric: "#{@metric_prefix}#{key}",
        value: value,
        timestamp: last_reported_at.to_i,
        tags: {
          server_id: @server.id,
          reporting: (@response_server["reporting"] ? 'yes' : 'no'),
          health_status: @response_server["health_status"]
        }
      )
    end
  end

  private

  def already_saved_data?
    metric = @response_server["summary"].keys.first
    prefix = ClientServer::TSD_PREFIX
    response = tsd.query_last(prefix + metric).parsed_response
    return false if response.is_a?(Hash) && response["error"].present?
    response.first && response.first["timestamp"] == last_reported_at.to_i
  end

  def server_not_reporting?
    not_reporting = @response_server["reporting"] == false
    Airbrake.notify('server not reporting!', server_id: @server.id) if not_reporting
    not_reporting
  end

  def last_reported_at
    if @response_server["last_reported_at"].present?
      Time.parse(@response_server["last_reported_at"])
    end
  end

  def tsd
    @tsd ||= TsdApi.new
  end
end
