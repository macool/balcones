ActiveAdmin.register ClientKeepalive do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :name, :uri, :searchterm, :slackwebhook, :client_id

  form do |f|
    f.inputs do
      f.input :name
      f.input :uri
      f.input :searchterm
      f.input :slackwebhook
      f.input :client
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :client
    column :name
    column :uri
    column :searchterm
    column :status
    column :created_at
    actions
  end

  action_item :trigger_ping, only: :show do
    link_to 'Trigger ping', trigger_ping_admin_client_keepalife_path(resource), method: :put
  end

  member_action :trigger_ping, method: :put do
    resource.schedule_ping!
    redirect_to admin_client_keepalife_path(resource), notice: "ping scheduled!"
  end
end
